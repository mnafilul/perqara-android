<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>android.widget.TextView - kur.si</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.TextView</value>
      <webElementGuid>8e5de85d-e7db-4c34-8f6d-fd9e6777cb90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>9a0f3fdc-ee47-4de6-ba97-607f1c416695</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>kur.si
bentuk tidak baku: korsi

1. n tempat duduk yang berkaki dan bersandaran

2. n ki kedudukan, jabatan (dalam parlemen, kabinet, pengurus, dan sebagainya): ia terpilih menduduki -- ketua

Gabungan kata: kursi bakso; kursi bar; kursi berlengan; kursi besar berlengan; kursi empuk; kursi ergonomis; kursi geser; kursi goyang; kursi hidraulik; kursi lincak; kursi lipat; kursi listrik; kursi lontar; kursi makan; kursi malas; kursi panas; kursi panjang; kursi pesakitan; kursi rebah; kursi roda; kursi setel; kursi sudut; kursi susun; kursi sutradara; kursi telur

Kur.si
bentuk tidak baku: korsi

n Isl ilmu atau kekuasaan Allah Swt.</value>
      <webElementGuid>cd056357-6f15-430b-b66f-76245eecce9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>yuku.kbbi5:id/tDesc</value>
      <webElementGuid>3b0e7cbe-8d31-4163-bc28-a39ec0c85970</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>yuku.kbbi5</value>
      <webElementGuid>dc584d35-8af5-4b27-a6d4-734946786e03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>81f56d65-6063-479a-bc9e-27d2f4851b9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>dde0a911-34bb-4c33-a85f-a63beb6f7b71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>95d7bcb8-94e3-42f3-9b56-18b264e725bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>2ed77f93-9ab2-401a-aedd-d9d34d8b567e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9ad8fe70-d753-41ad-8899-89b0c8b1ab99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>2e649a8e-7a31-4ffd-b699-673fc005aace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>b4fead0f-5aaa-46a4-a36f-c9ca2abc3377</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>53aa0dce-cf13-47e5-9b40-cf41365b2999</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>bf30d0e7-47f9-4e2e-951e-fea5a6f35bae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>96b056f5-ca73-4112-b4ba-66790de8862e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>42</value>
      <webElementGuid>6567c695-5f56-4c37-978d-ec9e63298b78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>252</value>
      <webElementGuid>31a91553-264d-41eb-8928-2b8d45e66697</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>985</value>
      <webElementGuid>e9df1c97-5f5f-4668-b9f5-d42780a5403f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1404</value>
      <webElementGuid>2d9781e7-56f8-4be2-bd34-d7441cf0e646</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[42,252][1027,1656]</value>
      <webElementGuid>99a669b7-928c-41f2-89d7-bb4c1fc17ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3a8710f8-88d6-4135-a09d-376a37d55b2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/androidx.drawerlayout.widget.DrawerLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/androidx.viewpager.widget.ViewPager[1]/android.widget.ScrollView[1]/android.widget.TextView[1]</value>
      <webElementGuid>18db608f-e522-41e8-8d8c-c195547117be</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.TextView' and (@text = 'kur.si
bentuk tidak baku: korsi

1. n tempat duduk yang berkaki dan bersandaran

2. n ki kedudukan, jabatan (dalam parlemen, kabinet, pengurus, dan sebagainya): ia terpilih menduduki -- ketua

Gabungan kata: kursi bakso; kursi bar; kursi berlengan; kursi besar berlengan; kursi empuk; kursi ergonomis; kursi geser; kursi goyang; kursi hidraulik; kursi lincak; kursi lipat; kursi listrik; kursi lontar; kursi makan; kursi malas; kursi panas; kursi panjang; kursi pesakitan; kursi rebah; kursi roda; kursi setel; kursi sudut; kursi susun; kursi sutradara; kursi telur

Kur.si
bentuk tidak baku: korsi

n Isl ilmu atau kekuasaan Allah Swt.' or . = 'kur.si
bentuk tidak baku: korsi

1. n tempat duduk yang berkaki dan bersandaran

2. n ki kedudukan, jabatan (dalam parlemen, kabinet, pengurus, dan sebagainya): ia terpilih menduduki -- ketua

Gabungan kata: kursi bakso; kursi bar; kursi berlengan; kursi besar berlengan; kursi empuk; kursi ergonomis; kursi geser; kursi goyang; kursi hidraulik; kursi lincak; kursi lipat; kursi listrik; kursi lontar; kursi makan; kursi malas; kursi panas; kursi panjang; kursi pesakitan; kursi rebah; kursi roda; kursi setel; kursi sudut; kursi susun; kursi sutradara; kursi telur

Kur.si
bentuk tidak baku: korsi

n Isl ilmu atau kekuasaan Allah Swt.') and @resource-id = 'yuku.kbbi5:id/tDesc']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
